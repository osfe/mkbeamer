# this a Makefile
BN = %PLACEHOLDER%
PDF = $(BN).pdf
TEX = $(BN).tex
PS = $(BN).ps
CC = pdflatex
CFLAGS = -interaction=nonstopmode

all: compile embedfont

compile: $(TEX)
	$(CC) $(CFLAGS) $<

embedfont: $(PDF)
	pdf2ps $(PDF) $(PS) && \
	gs -dSAFER -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sPAPERSIZE=a4 -dPDFSETTINGS=/printer \
	-dCompatibilityLevel=1.3 -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true \
	-sOutputFile=$(PDF) $(PS)

pdf:
	acroread datasheetEnglish.pdf

rtf:
	latex2rtf datasheetEnglish.tex

clean:
	find ./ -name '*.out' -exec rm -fR {} \; 	&& \
	find ./ -name '*.backup' -exec rm -fR {} \; 	&& \
	find ./ -name '*.toc' -exec rm -fR {} \;	&& \
	find ./ -name '*.aux' -exec rm -fR {} \;	&& \
	find ./ -name '*.lof' -exec rm -fR {} \;	&& \
	find ./ -name '*.lot' -exec rm -fR {} \;	&& \
	find ./ -name '*.log' -exec rm -fR {} \;	&& \
	find ./ -name '*.*~' -exec rm -fR {} \;

